import { FilterButton } from '../../common/components/FilterButton'

export interface QuestionType {
  id: number
  name: string
  displayNewestFlag: 0 | 1
}

export class QuestionTypeModel implements QuestionType {
  id: number = 0
  name: string = ''
  displayNewestFlag: 0 | 1 = 0

  constructor(model: QuestionType) {
    this.id = model.id
    this.name = model.name
    this.displayNewestFlag = model.displayNewestFlag
  }
}

export class FilterParams {
  // 排序：0 默认 20 浏览量↑ 21 浏览量↓
  sort: number = 0
  // 阶段ID
  index: number = 0
}


@Component
export struct QuestionFilterComp {
  // controller: CustomDialogController
  @Prop
  questionTypeList: QuestionTypeModel[] = []
  @Prop
  params: FilterParams = new FilterParams()
  // onSubmit
  onSubmit: (params: FilterParams) => void = () => {
  }

  @Builder
  TitleBuilder(title: string) {
    Text(title)
      .padding({ top: 10, bottom: 10 })
      .fontSize(14)
      .fontWeight(500)
  }

  build() {
    Column() {
      Row() {
        Text('重置')
          .fontSize(14)
          .onClick(() => {
            this.params = new FilterParams()
          })
        Text('筛选题目')
          .layoutWeight(1)
          .textAlign(TextAlign.Center)
        Text('完成')
          .fontSize(14)
          .fontColor($r('app.color.common_main_color'))
          .onClick(() => {
            this.onSubmit(this.params)
            // this.controller.close()
          })
      }
      .width('100%')
      .height(50)

      this.TitleBuilder('题目排序')
      Flex() {
        FilterButton({ text: '默认', isActive: this.params.sort === 0 })
          .onClick(() => this.params.sort = 0)
        FilterButton({ text: '浏览量', isSort: true, isActive: this.params.sort > 0, sortValue: this.params.sort })
          .onClick(() => {
            if (this.params.sort) {
              this.params.sort = this.params.sort === 21 ? 20 : 21
            } else {
              this.params.sort = 21
            }
          })
      }

      this.TitleBuilder('选择阶段')
      Flex({ wrap: FlexWrap.Wrap }) {
        ForEach(this.questionTypeList, (item: QuestionTypeModel, index: number) => {
          FilterButton({ text: item.name, isNew: item.displayNewestFlag === 1, isActive: this.params.index === index })
            .onClick(() => this.params.index = index)
        })
      }

    }
    .backgroundColor(Color.White)
    .padding({ left: $r('app.float.common_space16'), right: $r('app.float.common_space16') })
    .width('100%')
    .height(420)
    .borderRadius({ topLeft: 16, topRight: 16 })
    .alignItems(HorizontalAlign.Start)
    // .transition(TransitionEffect.move(TransitionEdge.BOTTOM)
    //   .animation({ duration: 300, curve: curves.springMotion(0.6, 0.8) }))
  }
}